<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\User;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;


class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {

            $data['status'] = false;
            $data['message'] = $validator->errors()->first();
            return response()->json($data);
        }

        try {
            DB::beginTransaction();

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            $token = $user->createToken('TutsForWeb')->accessToken;
            $response = ['status' => true, 'message' => 'Success', 'token' => $token];
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        return response()->json($response);
    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            $user = auth()->user();
            $token = $user->createToken('TutsForWeb')->accessToken;
            $response = ['status' => true, 'message' => 'Success', 'data' => $user, 'token' => $token];
            return response()->json($response, 200);
        } else {
            return response()->json(['status' => false, 'message' => 'Unauthorised User'], 401);
        }
    }

    /**
     * Handle Logout Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status' => true,
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Handle Change Password Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    function changePassword(Request $request)
    {
        $data = $request->all();
        $user = auth()->user();

        $validator = Validator::make($request->all(), [
            'newPassword' => 'required|min:6',
            'oldPassword' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }

        $isValidUser = false;
        if (Auth::guard('web')->attempt(['email' => $user->email, 'password' => $request->get('oldPassword')], false, false)) {
            $isValidUser = true;
        }

        try {
            if ($isValidUser) {
                DB::beginTransaction();
                $user->password = bcrypt($data['newPassword']);
                $user->save();
                $user->token()->revoke();
                $token = $user->createToken('newToken')->accessToken;
                $user->save();
                $response = ['status' => true, 'message' => 'Successfully Changed Password', 'token' => $token];
                DB::commit();
            } else {
                $response = ['status' => false, 'message' => 'Password does not matched'];
            }
        } catch (Exception $e) {
            DB::rollback();
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        return response()->json($response);
    }

    /**
     * Handle Forget Password
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function forgetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }

        $email = $request->email;
        $user = \Illuminate\Foundation\Auth\User::where('email', $email)->orWhere('user_name', $request->name)->get();
        $newPassword = str_random(6);

        try {
            if ($user->count() > 0) {
                $subject = 'Password Recovery';
                $messageBody = "Your new password is : $newPassword";
                $toChange = ['password' => bcrypt($newPassword)];

                DB::beginTransaction();
                User::where('email', $email)->update($toChange);
                mail($email, $subject, $messageBody);
                $response = ['status' => false, 'password' => $newPassword, 'message' => 'Success. Please check your email & login again.'];
                DB::commit();
            } else {
                $response = ['status' => false, 'message' => 'User not found'];
            }
        } catch (Exception $e) {
            DB::rollback();
            $response = ['status' => false, 'message' => $e->getMessage()];
        }
        return response()->json($response);
    }

    /**
     * Get User Information
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function userInfo(Request $request)
    {
        $user = $request->user();
        $data = [
            'id' => $user->id,
            'user_name' => $user->user_name,
            'email' => $user->email,
            'role_id' => $user->role_id,
            'user_photo' => $user->user_photo,
            'status' => $user->status,
        ];
        return response()->json($data);
    }

    /**
     *  User Information Update
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function userInfoUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'message' => $validator->errors()->first()]);
        }

        try {
            DB::beginTransaction();

            $name = $request->name;
            $email = $request->email;
            $user_photo = $request->file('user_photo');

            $user = auth()->user();

            if ($name) $user->user_name = $name;
            if ($email) $user->email = $email;

            if ($user_photo) {
                $imageName = md5(str_random(30) . time() . '_' . $user_photo) . '.' . $user_photo->getClientOriginalExtension();
                $user_photo->move('uploads/user_photo/', $imageName);

                if (file_exists('uploads/user_photo/' . $user->user_photo) AND !empty($user->user_photo)) {
                    unlink('uploads/user_photo/' . $user->user_photo);
                }
                $user->user_photo = $imageName;
            }
            $user->save();

            $data['status'] = true;
            $data['message'] = 'Success';
            $data['data'] = $user;

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            $data['status'] = false;
            $data['message'] = $e->getMessage();
        }

        return response()->json($data);
    }
}
