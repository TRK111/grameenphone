<?php

namespace App\Http\Controllers;

use App\Model\CompanyInfo;
use App\Model\Vehicle;
use App\Model\VehicleLocation;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class API extends Controller
{
    function index()
    {
        return view('welcome');
    }

    function hit_api()
    {
        /*$locations = VehicleLocation::get();

        dd($locations);*/

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');

        echo $response->getStatusCode(); # 200
        echo $response->getHeaderLine('content-type',''); # 'application/json; charset=utf8'
        echo $response->getBody(); # '{"id": 1420053, "name": "guzzle", ...}'

        # Send an asynchronous request.
        $request = new \GuzzleHttp\Psr7\Request('GET', 'http://httpbin.org');
        $promise = $client->sendAsync($request)->then(function ($response) {
            echo 'I completed! ' . $response->getBody();
        });

        $promise->wait();
    }

    function getTokenSampleReq()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://apigw.grameenphone.com/oauth/v1/token');

        $headers = [
            'Accept' => 'application/json',
            'Content-type' => 'application/x-www-form-urlencoded',
            //'Authorization' => 'Basic ' . base64_encode($username . ':' . $password),
        ];

        $data = (object) [
            'client_id' => 'Co1NDMtrmvPi2ODBixR3fakODQNX9IDG',
            'client_secret' => 'YRZNijLpOrmahdyv',
            'grant_type' => 'client_credentials',
        ];

        $json_dt = json_encode($data);

        $StatusCode = $response->getStatusCode();

        echo $response->getHeaderLine($headers);

        echo $response->getBody($json_dt);

    }

    function SMS_API()
    {
        $Client = new Client([
           'headers' => [
                            'Accept' => 'application/json',
                        ],
        ]);

        $response = $Client->request('POST','https://apigw.grameenphone.com/sms/v1/customers/01670974776/notifications',
            [
                'json' => [
                    'idType' => "MSISDN",
                    'messageText' => "TEXT",
                    'senderId' => "0130900246",
                    'language' => "EN",
                    'referenceCode' => "3547536",
                ]
            ]);

        $data = $response->getBody();
        $data = json_decode($data);

        dd($data);
    }

    function take_input(Request $request)
    {
        if ($request)
        {
            $data = [
              'vehicle_id' => $request->vehicle_id,
              'lat' => $request->lat,
              'long' => $request->long,
            ];

            try{

                VehicleLocation::insert($data);

                DB::commit();

                $msg['status'] = true;
                $msg['message'] = 'Success';

            }
            catch (\Exception $e)
            {
                DB::rollback();
                $msg['msg'] = $e->getMessage();
                $msg['status'] = false;
                $msg['message'] = 'Failed';
            }

            return response()->json($msg);
        }
    }

    function getData()
    {
        $company = CompanyInfo::get();
        $Vehicle = Vehicle::get();

        $data['company_infos'] = $company;
        $data['vehicles'] = $Vehicle;

        return response()->json($data);
    }

    function samples()
    {
        $url = "";

        $client = new \GuzzleHttp\Client();

        //GET request

        $request = $client->get($url, array('Accept' => 'application/json'));

        $response = $request->send();

        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300)
        {
            // Error
        }

        //END

        // POST with basic auth
        $myObj = [];

        $myObj->name = "John";
        $myObj->age = 30;
        $myObj->city = "New York";

        $myJSON = json_encode($myObj);

        $data = $myJSON;

        $username = '';
        $password = '';

        $headers = [
            'Content-type' => 'application/json; charset=utf-8',
            'Accept' => 'application/json',
            'Authorization' => 'Basic ' . base64_encode($username . ':' . $password),
        ];
        $response = $client->post($url, $headers, json_encode($data))->send();

        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300)
        {
            // Error
        }
        else{

        }
    }




}
