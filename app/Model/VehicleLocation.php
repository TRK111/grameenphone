<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VehicleLocation extends Model
{
    protected $fillable = [
        'id',
        'vehicle_id',
        'company_id',
        'lat',
        'long',
        'capture_time',
        'speed',
        'created_at',
        'updated_at'
    ];

}
