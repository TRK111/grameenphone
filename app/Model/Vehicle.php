<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'id',
        'vehicle_name',
        'details',
        'created_at',
        'updated_at'
    ];
}
