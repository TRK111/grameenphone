<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{
    protected $fillable = [
        'id',
        'name',
        'mobile',
        'email',
        'web',
        'address',
        'status',
        'created_at',
        'updated_at'
    ];
}
